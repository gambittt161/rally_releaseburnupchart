Ext.define('ReleaseBurnUpChart', {
    extend: 'Rally.app.App',
    componentCls: 'app',
    _isDebugMode: false,
    launch: function() {
      if(this._isDebugMode) {
        console.log("ReleaseBurnUpChart.launch called");
      }

      var self = this;

      var store = Ext.create('Rally.data.wsapi.Store', {
        model: 'release',
        autoLoad: true,
      //  context: this.getContext()
      });

      self.add(
        {
          xtype: 'rallyreleasecombobox',
          store: store,
          id: 'releaseComboBox',
          listeners: {
            select: {fn: this._refreshChart, scope: this},
          }
        },
        {
          xtype: 'rallycheckboxfield',
          fieldLabel: 'By Story Points',
          id: 'byStoryPointCheckBox',
          value: false,
          listeners: {
            change: {fn: this._refreshChart, scope: this}
          }
        },
        {
          xtype: 'rallynumberfield',
          fieldLabel: 'Anticipated Sprint Velocity Override',
          id: 'manualVelocityOverrideValueTextBox',
        },
        {
          xtype: 'rallynumberfield',
          fieldLabel: 'Projected Sprint Velocity',
          id: 'projectedSprintVelocity',
          readOnly: true
        },
        {
          xtype: 'rallybutton',
          text: 'Calculate',
          id: 'recalculateProjectionButton',
          value: false,
          listeners: {
            click: {fn: this._overrideProjections, scope: this}
          }
        }
      );

      if(this._isDebugMode) {
        console.log("ReleaseBurnUpChart.launch finished");
      }
    },
    _refreshChart: function () {
      this.down('#manualVelocityOverrideValueTextBox').setValue(null);
      this._loadReleaseBurndownChart();
    },
    _overrideProjections: function () {
      this._loadReleaseBurndownChart(true);
    },
    _loadReleaseBurndownChart: function (overrideProjections) {
      if(this._isDebugMode) {
        console.log("ReleaseBurnUpChart._loadReleaseBurndownChart called");
      }

      var comboBox = this.down('#releaseComboBox');
      var byStoryPointCheckBox = this.down('#byStoryPointCheckBox');
      var manualVelocityOverrideValue = this.down('#manualVelocityOverrideValueTextBox').getValue();

      var projectionStartIndex = null;

      if(this.down('#burndownChart')) {
          var chart = this.down('#burndownChart');
          var c = chart.getChart();
          if(c !== null) {
            var selectedPoints = c.getSelectedPoints();

            if(selectedPoints.length > 0 && overrideProjections === true) {
              projectionStartIndex = selectedPoints[0].x;
            }
          }

          this.remove('burndownChart');
        }

      var release = comboBox.getRecord().data;
      var burnByCount = byStoryPointCheckBox.getValue() === false;

      // if a Stage Push date is associated with the release, use it to generate the burndown instead of the Release EndDate.
      var endDate = (release.c_StageDate !== undefined && release.c_StageDate !== null) ? release.c_StageDate: release.ReleaseDate;

      this.add({
        xtype: 'rallychart',
        id: 'burndownChart',
        storetype: 'Rally.data.lookback.SnapshotStore',
        storeConfig: this._getStoreConfig(release.ObjectID),
        height: 500,
        calculatorType: 'BurndownCalculator',
        calculatorConfig: this._getCalculatorConfig(release.ReleaseStartDate, endDate, burnByCount, projectionStartIndex, manualVelocityOverrideValue),
        chartConfig: this._getChartConfig(burnByCount),
        listeners: {
          chartRendered: {fn: this._releaseTrackingChartRendered, scope: this}
        }
      });

      if(this._isDebugMode) {
        console.log("ReleaseBurnUpChart._loadReleaseBurndownChart finished");
      }
    },
    _releaseTrackingChartRendered: function (chart, options) {

      if(this._isDebugMode) {
        console.log("ReleaseBurnUpChart._releaseTrackingChartRendered called");
      }
      var projectionDetails = chart.calculator.projectionsConfig.series[0];
      var slope = projectionDetails.slope;
      var burnPerSprint = slope * 10;

      var projectedSprintVelocityNumericField = this.down('#projectedSprintVelocity');
      projectedSprintVelocityNumericField.setValue(burnPerSprint);

      if(this._isDebugMode) {
        console.log("ReleaseBurnUpChart._releaseTrackingChartRendered finished");
      }

    },
    _getCalculatorConfig: function (startDate, endDate, burnByCount, projectionsStartIndex, manualVelocityOverrideValue) {
      var config = {
        completedScheduleStateNames: ['Accepted', 'Completed'],
        startDate: startDate,
        endDate: endDate,
        burnByCount: burnByCount,
        metrics: [ // need to put the default metrics in the calculator
          {
            as: 'Scope',
            display: 'area',
            f: (burnByCount)? 'count' : 'sum',
            field: (burnByCount)? null : 'PlanEstimate'
          },
          {
            as: 'ToDo',
            display: 'line',
            f: 'sum',
            field: (burnByCount)? 'StoriesRemaining' : 'StoryPointsRemaning'
          },
          {
            as: 'Completed',
            display: 'line',
            f: 'sum',
            field: (burnByCount)? 'StoriesCompleted' : 'StoryPointsCompleted'
          },
          {
            f: 'count',
            display: 'line',
            field: 'CompletedProjection'
          }
        ],
        //todo store the holidays in a google sheet and pull the data.  It can be reused for multliple reports without duplication
        holidays: [
          {"year": 2017, "month": 11, "day": 23},
          {"year": 2017, "month": 11, "day": 24},
          {"year": 2017, "month": 12, "day": 25},
          {"year": 2017, "month": 12, "day": 26},
          {"year": 2018, "month": 1, "day": 1}
        ],
        projectionsConfig: {
        continueWhile: function(point, scope) {
          return point.CompletedProjection < scope;
        },
          series: [
            {as: 'CompletedProjection', field: 'Completed'}
          ],
          //limit: 50 // haven't implemented the limit yet.
        }
      };

      if(projectionsStartIndex !== undefined && projectionsStartIndex !== null) {
        config.projectionsConfig.series[0].startIndex = projectionsStartIndex;
      }

      if(manualVelocityOverrideValue !== undefined && manualVelocityOverrideValue !== null) {
        // since we work in 2 week sprints, assume the user is giving velocity in sprints.  That would equate to 10 working days
        config.projectionsConfig.series[0].slope = manualVelocityOverrideValue/10 ;
      }

      return config;
    },
    _getStoreConfig: function (releaseObjectID) {
        return [
          {
            find: {
              "$or": [
                {"_TypeHierarchy": "HierarchicalRequirement"}
              ],
              'Release' : releaseObjectID
            },
            fetch: ["ScheduleState", "c_StageDate", "PlanEstimate"],
            hydrate: ["ScheduleState"]
          }
        ];
      },
    _getChartConfig: function(burnByCount) {
      var config = {
        title: {
          text: 'Release Tracking'
        },
        xAxis: {
          tickmarkPlacement: 'on',
          tickInterval: 10
        },
        yAxis: [],
        chart: {
          zoomType: 'x'
        },
        plotOptions: {
          series: {
            allowPointSelect: true
          }
        }
      };

      if(!burnByCount) {
        config.yAxis.push({title: { text: 'Story Points'}, min: 0});
      //  config.yAxis.push({ title: { text: 'Scope (by Story Count)'}, opposite: true });
      } else {
        config.yAxis.push({title: {text: 'Story Count'}, min: 0});
      }

      return config;
    }
});
